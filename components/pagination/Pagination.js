import PaginationResult from './PaginationResult.js'
import PaginationLinks from './PaginationLinks.js'
import { POSTS_PER_PAGE } from '../../config/index.js'

export default function Pagination({blogsCount, firstChunk, lastChunk, handlePagClick}) {
  return (
    <div className="bg-white px-4 py-3 flex flex-wrap items-center justify-between border-t border-gray-200 sm:px-6 space-y-4">
        <div className="sm:hidden w-full">
          <PaginationResult blogsCount={blogsCount} firstChunk={firstChunk} lastChunk={lastChunk} />
        </div>
        
        {blogsCount > POSTS_PER_PAGE &&
          <div className="flex-1 flex justify-between sm:hidden">
            <PaginationLinks withChevrons={false} handlePagClick={handlePagClick}/>
          </div>
        }
      <div className="hidden sm:block sm:items-center sm:justify-between sm:space-y-4">
        <div>
          <PaginationResult blogsCount={blogsCount} firstChunk={firstChunk} lastChunk={lastChunk} />
        </div>
        <div>
          {blogsCount > POSTS_PER_PAGE && 
          <nav className="block relative z-0 inline-flex rounded-md space-x-4" aria-label="Pagination">
            <PaginationLinks withChevrons={true} handlePagClick={handlePagClick}/>
          </nav>
          }
        </div>
      </div>
    </div>
  )
}