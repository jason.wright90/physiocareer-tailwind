import React from 'react'

export default function PaginationResult({blogsCount, firstChunk, lastChunk}) {
  return (
    <div>
      {firstChunk > 0
        ?

        firstChunk !== lastChunk
          ?
            <p className="text-sm text-gray-700">
              Showing <span className="font-medium">{firstChunk}</span> to <span className="font-medium">{lastChunk}</span> of{' '}
              <span className="font-medium">{blogsCount}</span> results
            </p>

          :
            <p className="text-sm text-gray-700">
              Showing <span className="font-medium">{lastChunk}</span> of{' '}
              <span className="font-medium">{blogsCount}</span> results
            </p>
        : <p className="text-sm text-gray-700">No results to show</p>
      }
    </div>
  )
}
