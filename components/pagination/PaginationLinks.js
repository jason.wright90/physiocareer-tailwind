import React from 'react'
import { ChevronLeftIcon, ChevronRightIcon } from '@heroicons/react/solid'

export default function PaginationLinks({withChevrons, handlePagClick}) {

  return (
    <>
      <a
        href="#"
        className="pag-prev relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50"
        onClick={handlePagClick}
      >
        {withChevrons && <ChevronLeftIcon className="pag-prev h-5 w-5" aria-hidden="true" />}
        <span className="pag-prev">Previous</span>
      </a>
      <a
        href="#"
        className="pag-next ml-3 relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50"
        onClick={handlePagClick}
      >
        <span className="pag-next">Next</span>
        {withChevrons && <ChevronRightIcon className="pag-next h-5 w-5" aria-hidden="true" />}
      </a>
    </>
  )
}
