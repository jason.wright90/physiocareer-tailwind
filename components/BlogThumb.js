import styles from './BlogThumb.module.css'
import Badge from './Badge'
import Link from 'next/link'
import { useEffect, useState } from 'react'

export default function BlogThumb({ blog, showBlogThumb }) {

  const [date, setDate] = useState(null)
  useEffect(()=>setDate(new Date(blog.date).toLocaleString('en-UK', {day: 'numeric', month: 'short', year: 'numeric'}), []))

  const handleBadgeClick = (e) => {e.preventDefault()}

  return (
    <div className={showBlogThumb ? styles.show + " grid" : styles.hide + " grid" }>
      <div className="grid shadow-md py-4 px-4 rounded-md border-2">

        <div className="mb-4">
          <img className="object-cover rounded-md md:h-52 w-full h-full sm:w-80" src={blog.image.formats.thumbnail.url} alt="blog image"/>
        </div>

        <h3 className="font-semibold my-3 text-blue-600 text-lg capitalize">{blog.title}</h3>


        <div className="flex self-start">
          <div className="text-sm font-medium mr-4">{date}</div>
          <Badge handleBadgeClick={handleBadgeClick} text={blog.category} selected={true} />
        </div>
          
        <div className="self-start mt-2">
          <p className="leading-snug text-sm">{blog.strapline}</p>
        </div>
        
        <Link href={`/blog/${blog.slug}`}>
          <a className="bg-blue-500 mt-4 py-3 px-8 rounded-md text-white w-full self-end inline-block text-center">Read More</a>
        </Link>
      </div>
    </div>
  )
}
