import React from 'react'

export default function Badge({ text, handleBadgeClick, selected, cursor}) {

  return (
    <span
      onClick = {(e)=>handleBadgeClick(e)}
      className={
        selected  
                  ? `text-xs px-2 font-medium rounded py-0.5 ${cursor} bg-gray-500 text-white border-2 border-gray-500`
                  : `text-xs px-2 font-medium rounded py-0.5 ${cursor} bg-white text-gray-500 border-2 border-gray-500`
        }    >
      {text}
    </span>
  )
}

Badge.defaultProps = {
  cursor: "cursor-auto",
}