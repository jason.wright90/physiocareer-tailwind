import Badge from "./Badge"
import BlogThumb from "./BlogThumb"
import Pagination from "./Pagination"
import { useEffect, useState } from "react"

export default function BlogSection({ blogs }) {
  
  const filterObjectByValue = (object, value) => {
    return Object.keys(object).filter(key=>object[key] === value)
  }

  const categories = [...new Set(blogs.map(blog => blog.category))]
  const categoriesObjFalse = Object.fromEntries(categories.map(category => [category, false]))
  const categoriesObjTrue = Object.fromEntries(categories.map(category => [category, true]))

  const [badges, setBadges] = useState(categoriesObjTrue)
  const [badgeFilter, setBadgeFilter] = useState(filterObjectByValue(badges, true))
  
  const [blogPosts, setBlogPosts] = useState({blogs}.blogs)
  const [blogsChunk, setBlogsChunk] = useState(blogPosts.slice(0,3))
  const [blogsCount, setBlogsCount] = useState(blogPosts.length)
  const [page, setPage] = useState(0)

  const sliceIntoChunks = (arr, chunkSize) => {
    const res = [];
    for (let i = 0; i < arr.length; i += chunkSize) {
        const chunk = arr.slice(i, i + chunkSize);
        res.push(chunk);
    }
    return res;
  }

  useEffect(()=>{setBlogsChunk(sliceIntoChunks(blogPosts,3)[page])},[])
  useEffect(() => { setBlogsCount(blogPosts.length) }, [])

  console.log(badges)
  console.log(badgeFilter)

  const handleBadgeClick = async (e) => {
    const badgeText = e.target.innerText

    setBadges({ ...badges, [badgeText]: !badges[badgeText] })
    setBadgeFilter(filterObjectByValue(badges, false))

    setBlogPosts({blogs}.blogs.filter(blog => badgeFilter.indexOf(blog.category) > -1))
    setBlogsCount(blogPosts.length)
    setBlogsChunk(blogPosts.slice(0, 3))
  }

  return (
    <div className="min-h-[calc(100%)] w-full px-4 sm:px-6 lg:px-8 text-gray-600 mb-12">
      <h2 className="text-5xl font-bold mb-8">Blog</h2>

      <h4 className="text-xl font-medium">Tags</h4>
      <span className="text-xs mb-4 inline-block italic">Click to filter</span>
      <div className="flex space-x-4 mb-8">
        {categories.map(category => (
          <Badge handleBadgeClick={handleBadgeClick} selected={badges[category]} text={category} cursor="cursor-pointer" key={category} />
        ))}
      </div>
      
      <div className="grid gap-16 grid-cols-1 rid-rows-1 sm:grid-cols-2 lg:grid-cols-3 mb-8">
        {blogsChunk
          .filter(blog => badgeFilter.indexOf(blog.category) > -1)
          .map(blog => (
          <BlogThumb blog={blog} key={blog.id} />
        ))}
      </div>

      <Pagination
        blogsCount={blogsCount}
        firstChunk={(page*3 + 1)}
        lastChunk={(page+1)*3}
      />

    </div>
  )
}

