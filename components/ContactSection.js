export default function ContactSection() {
  return (
    <section className="h-full w-full px-4 sm:px-6 lg:px-8 text-gray-600 flex flex-col justify-center" id="contact">
      <h2 className="text-5xl font-bold ">Book Coaching</h2>
      <form className="mt-8 bg-white shadow-md rounded px-8 pt-6 pb-8 mb-2">
        <div className="mb-4">
          <label
            className="block text-gray-700 font-bold mb-2"
            htmlFor="name">
            Name
          </label>
          <input
            className="shadow appearance-none border rounded w-full 
            py-2 px-3 text-gray-700 leading-tight 
            focus:border-blue-500 focus:outline-none focus:shadow-outline"
            placeholder="Enter your name"
            type="text"
            name="name">
          </input>
        </div>
        <div className="mb-4">
          <label
            className="block text-gray-700 font-bold mb-2"
            htmlFor="service">
            What would you like to improve at?
          </label>
          <select
            className="shadow appearance-none border rounded w-full 
            py-2 px-3 text-gray-700 leading-tight 
            focus:border-blue-500 focus:outline-none focus:shadow-outline"
            name="service">
            <option value="-">-</option>
            <option value="HCPC Application">HCPC Application</option>
            <option value="Job Applications">Job Applications</option>
            <option value="Interview Technique">Interview Technique</option>
          </select>
        </div>
        <div className="mb-8">
          <label
            className="block text-gray-700 font-bold mb-2"
            htmlFor="message">
            Message
          </label>
          <textarea
            className="form-control block w-full
            px-3 py-1.5 h-32 text-base
            font-normal text-gray-700
            bg-white bg-clip-padding
            border border-solid border-gray-300
            rounded shadow 
            transition ease-in-out
            m-0 focus:text-gray-700 focus:bg-white focus:border-blue-500 focus:outline-none
            "
            placeholder="Enter your message"
            name="message">  
          </textarea>
        </div>
        <button
          className="bg-blue-500 text-white font-bold 
          py-2 px-4 rounded focus:outline-none focus:shadow-outline"
          type="button"
        >
          Submit
        </button>
      </form>
    </section>
  )
}