import Badge from "./Badge"
import BlogThumb from "./BlogThumb"
import Pagination from "./pagination/Pagination"
import { useEffect, useState } from "react"
import { POSTS_PER_PAGE, FADE_DURATION } from '../config/index.js'

export default function BlogSection({ blogs }) {

  /*  1.  Create tags from blogs categories
      2.  Based on whether a tag is selected or not, render the blogs
          We should useEffect with the tag state being a condition to re-render blogs

      Pagination
        Simple
        - Prev ... Next

        Complex
        - Create a page link for each page between 1 and total number of pages
        - Show Start <- 1 - 3 ... (last page - 2) - last page -> End
        - When any element is clicked, set it as the initial value
        - Count number of pages by dividing total blogs by blogs per page (rounded up)
        - Set last page in pagination element to be total number of pages
  */
  
  const [tags, setTags] = useState([])
  const [allTags, setAllTags] = useState(true)
  const [page, setPage] = useState(1)
  const [blogPosts, setBlogPosts] = useState([])
  const [showBlogThumb, setShowBlogThumb] = useState(true)

  const firstChunk = POSTS_PER_PAGE * (page-1)
  const lastChunk = POSTS_PER_PAGE * page
  const totalPages = Math.ceil(blogPosts.length / POSTS_PER_PAGE)

  useEffect(() => {
    const getInitialCategories = async() => {
      let categories = await [... new Set((blogs.map(blog => (blog.category))))]
      setTags(categories.map(category => ({ category: category, selected: false })))
    }
    getInitialCategories();
  }, [])

  useEffect(() => {
    const selectedTags = tags.filter(tag => tag.selected).map(tag => tag.category)
    const filteredBlogPosts = allTags ? blogs : blogs.filter(blog=>selectedTags.indexOf(blog.category) > -1)
    setBlogPosts(filteredBlogPosts)
  },[allTags, tags, page])
  
  const handleBadgeClick = (e) => {

    setShowBlogThumb(false)
    
    const setTagSelections = () => {
      setAllTags(false)
      setPage(1)
      let category = e.target.innerText
      tags.forEach(tag => (tag.category === category) && setTags([...tags, tag.selected=!tag.selected].slice(0,tags.length)))
      setTimeout(()=>setShowBlogThumb(true), 200)
    }
    
    setTimeout(()=>setTagSelections(), 200)
  }

  const handleAllBadgeClick = (e) => {
    setShowBlogThumb(false)

    const resetCategories = async () => {
      setAllTags(true)
      setPage(1)
      let categories = await [... new Set((blogs.map(blog => (blog.category))))]
      setTags(categories.map(category => ({ category: category, selected: false })))
      setTimeout(()=>setShowBlogThumb(true), 200)
    }

    setTimeout(()=>resetCategories(), 200)
  }

  const handlePagClick = (e) => {
    e.preventDefault()
    setShowBlogThumb(false)
    setTimeout(() => {
      if (e.target.classList.contains('pag-prev')) {
        page === 1 ? setPage(totalPages) : setPage(page - 1)
      } else if (e.target.classList.contains('pag-next')) {
        page >= totalPages ? setPage(1) : setPage(page + 1)
      }
      setTimeout(()=>setShowBlogThumb(true),200)
    },200)
  }

  return (
    <section className="min-h-[calc(100%)] w-full px-4 sm:px-6 lg:px-8 text-gray-600 mb-12" id="blog">
      <h2 className="text-5xl font-bold mb-8">Blog</h2>
      <h4 className="text-xl font-medium">Tags</h4>
      <span className="text-xs mb-4 inline-block italic">Click to filter</span>
      <div className="flex space-x-4 mb-8">
        <Badge text="All" selected={allTags} cursor={"cursor-pointer"} handleBadgeClick={handleAllBadgeClick}/>
        {tags.map(tag => (
          <Badge
            key={tag.category}
            text={tag.category}
            selected={tag.selected}
            handleBadgeClick={handleBadgeClick}
            cursor={"cursor-pointer"}
          />))}
      </div>
      
      <div className="grid gap-16 grid-cols-1 rid-rows-1 sm:grid-cols-2 lg:grid-cols-3 mb-8">
          {blogPosts.map(blogPost => (
          <BlogThumb blog={blogPost} key={blogPost.id} showBlogThumb={showBlogThumb} />
          )).slice(firstChunk, lastChunk)}
      </div>

      <Pagination
        blogsCount = {blogPosts.length}
        firstChunk = {blogPosts.length > 0 ? POSTS_PER_PAGE * (page - 1) + 1 : 0}
        lastChunk={POSTS_PER_PAGE * page > blogPosts.length ? blogPosts.length : POSTS_PER_PAGE * page}
        handlePagClick={(e)=>handlePagClick(e)}
      />
    </section>
  )
}

