import { AiFillInstagram, AiFillLinkedin} from 'react-icons/ai'

export default function AboutSection({ about }) {
  return (
    <section className="w-full px-4 sm:px-6 lg:px-8 text-gray-600 mb-20" id="about">
      <h2 className="text-5xl font-bold mb-12">About</h2>
      
      <div className="w-full md:flex md:flex-row-reverse items-center justify-end" >

        <div className="mb-8 md:mb-0 md:ml-12 md:self-start text-lg w-3/4">
          <h3 className="text-3xl mb-8 font-semibold">Sub-title goes here </h3>
          <p>"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."</p>
        </div>

        <div className="bg-white rounded-md">          
          <img
            src={about.aboutavatar.formats.small.url}
            className="
              mb-6
              object-cover h-72 w-72 rounded-md
              shadow-md
            "
          />
          <div className="font-bold text-lg">Deborah Dias Do Rosario</div>
          <div className="font-semibold text-sm text-blue-600 mt-1">MSK Physiotherapist | NHS (Band 6)</div>
          <div className="text-gray-400 text-3xl mt-4 flex space-x-4 ml-[-2px]">
            <AiFillLinkedin className="hover:text-gray-500 hover:cursor-pointer"/>
            <AiFillInstagram className="hover:text-gray-500 hover:cursor-pointer"/>
          </div>
        </div>
      </div>
    </section>
  )
}