export default function LandingSection() {
  return (
    <div className="h-[calc(100%-6rem)] w-full flex flex-col-reverse md:flex-row px-4 sm:px-6 lg:px-8 space-x-12 items-center">
        <div className="md:h-full md:w-1/2 text-center md:text-left flex flex-col justify-center py-16">
          <p className="text-gray-600 leading-tight sm:text-3xl md:text-4xl lg:text-5xl font-bold text-2xl">
            Find work as a
          <span className="text-blue-600 "> Physio </span>in the UK
          </p>
          <p className="text-gray-600 md:text-xl py-6 leading-normal text-lg">
            Helping people across the globe to find work as a physiotherapist in the UK. <span className="text-blue-600 font-bold">Online coaching</span> to suit your needs, whether you're a student, postgraduate or taking the next step in your career.
          </p>
          <a className="bg-blue-500 text-white py-4 font-bold rounded text-center" href="#contact">
            Book Coaching
          </a>
        </div>
        <div className="md:h-full md:w-1/2 w-full flex md:justify-end justify-center items-center">
          <img className="md:object-cover rounded-md md:h-[19.5rem] md:w-full md:object-right md:-my-0 sm:-my-10" src="/imgs/bg_debwork.jpg"/>
        </div>
      </div>

  )
}