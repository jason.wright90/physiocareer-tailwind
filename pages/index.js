import { STRAPI_API } from '../config'
import Head from 'next/head'
import Nav from '../components/Nav'
import LandingSection from '../components/LandingSection'
import AboutSection from '../components/AboutSection'
import BlogSection from '../components/BlogSection'
import ContactSection from '../components/ContactSection'

export default function HomePage({blogs, about}) {
  return (
    <div className="max-w-7xl px-8 sm:px-6 lg:px-8 mx-auto h-screen">
      <Head>
        <title>Physio Career</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Nav/>
      <LandingSection />
      <AboutSection about={about}/>
      <BlogSection blogs={blogs}/>
      <ContactSection />

    </div>
  )
}

export async function getServerSideProps() {

  const blogsRes = await fetch(`${STRAPI_API}/blogs`)
  const blogs = await blogsRes.json()

  const aboutRes = await fetch(`${STRAPI_API}/about-section`)
  const about = await aboutRes.json()

  return {
    props: {
      blogs,
      about
    }
  }
}