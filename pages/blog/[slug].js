import React from 'react'
import ReactMarkdown from 'react-markdown'
import {AiOutlineHome, AiOutlineArrowRight, AiOutlineArrowLeft} from 'react-icons/ai'
import { STRAPI_API } from '../../config'

import styles from './Blog.module.css'

export default function BlogPage({blog, nextBlog, prevBlog}) {
  return (
    <div className="min-h-[calc(100%)] w-full"> 
      <div className="flex justify-center items-center">
        <img className="mix-blend-soft-light object-cover w-full h-80" src={blog.image.formats.large.url }/>
      </div>
      <div className="text-gray-600 mt-8 px-8 sm:px-24 md:px-36 lg:px-52 mb-20 w-full">
        <h1 className="text-3xl font-bold mb-3 text-center ">{blog.title}</h1>
        <div className="text-center md:px-8 lg:px-28">
          <p className="text-lg italic font-light mb-4">{blog.strapline}</p>
          <ul className="flex justify-center space-x-16 md:space-x-24 mt-8 mb-8">
            <li className="flex items-center space-x-2">
              <AiOutlineArrowLeft />
              <a href={`/blog/${prevBlog.slug}` }>Prev Post</a>
            </li>
            <li className="flex items-center space-x-2">
              <AiOutlineHome />
              <a href="/" className="inline-block">Home </a>
            </li>
            <li className="flex items-center space-x-2">
              <a href={`/blog/${nextBlog.slug}` }>Next Post</a>
              <AiOutlineArrowRight />
            </li>
          </ul>
          <ReactMarkdown children={blog.post} className={styles.blog} />
        </div>
      </div>
    </div>
  )
}

export async function getServerSideProps({ query: { slug } }) {

  const queryRes = await fetch(`${STRAPI_API}/blogs?slug=${slug}`)
  const queryBlogJSON = await queryRes.json()
  
  if (!queryBlogJSON) {
    return {
      notFound: true,
    }
  }

  const allBlogsRes = await fetch(`${STRAPI_API}/blogs`)
  const blogsJSON = await allBlogsRes.json()

  const ids = blogsJSON.map(blog => blog.id)
  const id = queryBlogJSON[0].id

  const nextBlog = id === Math.max(...ids)
    ? await (await (fetch(`${STRAPI_API}/blogs?id=1`))).json()
    : await (await (fetch(`${STRAPI_API}/blogs?id=${id + 1}`))).json()
  
  const prevBlog = id === Math.min(...ids)
    ? await (await (fetch(`${STRAPI_API}/blogs?id=${ids.length}`))).json()
    : await (await (fetch(`${STRAPI_API}/blogs?id=${id - 1}`))).json()
  
  return {
    props: {
      blog: queryBlogJSON[0],
      nextBlog: nextBlog[0],
      prevBlog: prevBlog[0]
    },
  }
}