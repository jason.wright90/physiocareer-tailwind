export const STRAPI_API = process.env.STRAPI_API || 'http://localhost:1337'
export const FRONTEND_URL = process.env.FRONTEND_URL || 'http://localhost:3000'
export const POSTS_PER_PAGE = 3
export const FADE_DURATION = 200;